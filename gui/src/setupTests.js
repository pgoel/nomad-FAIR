global.nomadEnv = {
  'keycloakBase': 'https://nomad-lab.eu/fairdi/keycloak/auth/',
  'keycloakRealm': 'fairdi_nomad_test',
  'keycloakClientId': 'nomad_gui_dev',
  'appBase': 'http://nomad-lab.eu/prod/rae/beta',
  'debug': false,
  'matomoEnabled': false,
  'matomoUrl': 'https://nomad-lab.eu/fairdi/stat',
  'matomoSiteId': '2',
  'version': {
    'label': '0.10.1',
    'isBeta': false,
    'isTest': true,
    'usesBetaData': true,
    'officialUrl': 'https://nomad-lab.eu/prod/rae/gui'
  },
  'encyclopediaEnabled': true,
  'aitoolkitEnabled': true,
  'oasis': false
}
