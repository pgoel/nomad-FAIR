Python Reference
================

nomad.metainfo
--------------
.. automodule:: nomad.metainfo.metainfo

nomad.config
------------
.. automodule:: nomad.config

nomad.infrastructure
--------------------
.. automodule:: nomad.infrastructure
    :members:

nomad.datamodel
---------------
.. automodule:: nomad.datamodel
    :members:

nomad.files
-----------
.. automodule:: nomad.files
    :members:

nomad.archive
-------------
.. automodule:: nomad.archive
    :members:

nomad.doi
---------
.. automodule:: nomad.doi
    :members:

nomad.parsing
-------------
.. automodule:: nomad.parsing

nomad.normalizing
-----------------
.. automodule:: nomad.normalizing
    :members:

nomad.processing
----------------
.. automodule:: nomad.processing

nomad.search
------------
.. automodule:: nomad.search
    :members:

nomad.app
---------
.. automodule:: nomad.app

nomad.cli
------------
.. automodule:: nomad.cli

nomad.client
------------
.. automodule:: nomad.client

nomad.utils
-----------
.. automodule:: nomad.utils

tests
-----
.. automodule:: tests
