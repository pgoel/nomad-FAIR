NOMAD Repository and Archive
============================

This project is a prototype for the continuation of the original NOMAD-coe software
and infrastructure with a simplyfied architecture and consolidated code base.

.. toctree::
   :maxdepth: 1

   introduction.md
   upload.md
   api.md
   client/client.rst
   metainfo.rst
   archive.rst
   developers.md
   parser.md
   normalizer.rst
   oasis.rst
   ops/ops.rst
   api_reference.rst
   python_reference.rst

.. # Introduction
..    # Repository, Encyclopedia, etc.
..    # Interfaces: GUIs and APIs
..    # Technical architecture
..    # History
.. # How to upload data
..    # Prepare your files
..    # Upload your files
..    # Publish you files
.. # How to use NOMAD APIs
..    # The different APIs
..    # curl
..    # requests
..    # bravado
.. # NOMAD's Python library
..    # Getting started
..    # Command line interface (CLI)
..    # Run parsers and normalizers
.. # Data format (Metainfo)
..    # Introduction
..    # Browsing the Metainfo
..    # Getting started
..    # Quantities
..    # Sections
..    # Reference
.. # Data access (Archive)
..    # Introduction
..    # Browsing the Archive
..    # Getting started
.. # Develop NOMAD
..    # Getting started
..    # Running NOMAD
..    # Running tests
..    # Style guide
..    # VS code
..    # GIT
.. # How to write a parser
..    # Getting started
..    # Text files
..    # XML parser
.. # How to write a normalizer
.. # Operating a NOMAD OASIS
.. # Operating NOMAD (with k8s)
.. # API Reference
.. # Python Reference
