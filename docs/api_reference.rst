API Reference
====================

This is just a brief summary of all API endpoints of the NOMAD API. For a more compelling documention
consult our *swagger* dashboards:

- (NOMAD API)[swagger dashboard](https://nomad-lab.eu/prod/rae/api/)
- (NOMAD's Optimade API)[swagger dashboard](https://nomad-lab.eu/prod/rae/optimade/)


Summary
-------

.. qrefflask:: nomad.app.flask:app
  :undoc-static:



API(s) Details
--------------

.. autoflask:: nomad.app.flask:app
  :undoc-static:
