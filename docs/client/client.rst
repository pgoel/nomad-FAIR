NOMAD's Python library
----------------------
The :code:`nomad` python package comes with a command line interface (CLI) that
can be accessed after installation by simply running the :code:`nomad` command
in your terminal. The CLI provides a hiearchy of commands by using the `click
package <https://click.palletsprojects.com/>`_.

This documentation describes how the CLI can be used to manage a NOMAD
installation. For commmon use cases see :ref:`cli_use_cases`. For a full
reference of the CLI commands see :ref:`cli_ref`.

.. toctree::
   :maxdepth: 2

   install.rst
   parsers.rst
   cli_use_cases.rst
   cli_ref.rst
