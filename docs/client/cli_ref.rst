.. _cli_ref:

CLI Reference
*************

Parse CLI
""""""""""""""""""""""""""""""""""""""
.. click:: nomad.cli.parse:_parse
  :prog: nomad parse
  :show-nested:

Client CLI commands
"""""""""""""""""""""""""""""""""""""""""
.. click:: nomad.cli.client.client:client
  :prog: nomad client
  :show-nested:

Admin CLI commands
""""""""""""""""""""""""""""""""""""""
.. click:: nomad.cli.admin.admin:admin
  :prog: nomad admin
  :show-nested:

