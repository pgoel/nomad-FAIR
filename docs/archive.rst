.. _access-the-archive-label:

Data Access (Archive)
=====================

Of course, you can access the NOMAD Archive directly via the NOMAD API (see the `API tutorial <api_tutorial.html>`_
and `API reference <api.html>`_). But, it is more effective and convenient to use NOMAD's Python client
library.

.. automodule:: nomad.client
